﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsGeneratorService.Models
{
    public class ExcelInputModel
    {
        public string Key { get; set; }

        public DateTime DateStarted { get; set; }

        public string UserName { get; set; }

        public double TimeSpent { get; set; }

        public string WorkDescription { get; set; }
    }
}
