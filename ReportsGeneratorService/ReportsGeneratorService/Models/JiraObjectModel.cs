﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsGeneratorService.Models
{
    public class JiraObjectModel
    {
        public int Total { get; set; }

        public List<IssueModel> Issues { get; set; }
    }

    public class IssueModel
    {
        public string Key { get; set; }

        public FieldModel Fields { get; set; }
    }

    public class FieldModel
    {
        public WorklogModel Worklog { get; set; }
    }

    public class WorklogModel
    {
        public List<WorklogsModel> Worklogs { get; set; }
    }

    public class WorklogsModel
    {
        public AuthorModel Author { get; set; }

        public string Comment { get; set; }

        public double TimeSpentSeconds { get; set; }

        public DateTime Started { get; set; }
    }

    public class AuthorModel
    {
        public string Name { get; set; }
    }
}
