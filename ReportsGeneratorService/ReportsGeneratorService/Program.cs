﻿using ReportsGeneratorService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportsGeneratorService
{
    public class Program
    {
        const string path = "C:\\ReportsGenerator\\ExcelTest.xls";

        static void Main(string[] args)
        {
            var startDate = DateTime.Now;
            var date = startDate.Date;
            while (date.DayOfWeek != DayOfWeek.Monday) date = date.AddDays(-1);

            DateTime endDate = date.AddDays(5);

            string pass = "";
            Console.Write("Enter your password: ");
            ConsoleKeyInfo key;

            do
            {
                key = Console.ReadKey(true);

                if (key.Key != ConsoleKey.Backspace)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    Console.Write("\b");
                }
            }
            while (key.Key != ConsoleKey.Enter);


            IExportService exportService = new ExportService();
            var excelInput = exportService.ConvertDataFromJiraToExcelInput("kamaci", pass, 14600, new DateTime(2017,10,22), new DateTime(2017,10,28));

            exportService.ExportToExcelFile(path, excelInput);

            Console.ReadLine();
        }
    }
}
