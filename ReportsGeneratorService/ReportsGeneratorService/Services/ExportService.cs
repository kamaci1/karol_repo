﻿using System;
using ReportsGeneratorService.Models;
using ReportsGeneratorService.Helpers;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.IO;

namespace ReportsGeneratorService.Services
{
    public class ExportService : IExportService
    {
        private JiraObjectModel _jiraObj;

        public List<ExcelInputModel> ConvertDataFromJiraToExcelInput(string userName, string password, int projectId, DateTime startDate, DateTime endDate)
        {
            //encode username:password
            password = password.Contains("\r") ? password.Replace("\r", "") : password;
            var encodedUP = EncodeUsernamePassword.Base64Encode($"{userName}:{password}");

            //build http authorization headers
            var httpClient = AuthenticationClient.Client();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encodedUP);
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //create the request
            var uri = $"https://asgroup.atlassian.net/rest/api/2/search?fields=worklog&jql=project=" +
                $"{projectId} and worklogDate > {startDate.ToString("yyyy-MM-dd")} and worklogDate < {endDate.ToString("yyyy-MM-dd")}";

            //send the request
            var response = httpClient.GetStringAsync(uri).Result;
            _jiraObj = JsonConvert.DeserializeObject<JiraObjectModel>(response);

            //filter the request, get rid of incorrect dates
            var worklogs = _jiraObj.Issues.Select(x => x.Fields.Worklog.Worklogs);
            List<ExcelInputModel> excelInput = new List<ExcelInputModel>();

            foreach (var issue in _jiraObj.Issues)
            {
                foreach (var worklog in issue.Fields.Worklog.Worklogs)
                {
                    if (worklog.Started <= endDate)
                    {
                        excelInput.Add(new ExcelInputModel
                        {
                            Key = issue.Key,
                            DateStarted = worklog.Started,
                            TimeSpent = worklog.TimeSpentSeconds,
                            UserName = worklog.Author.Name,
                            WorkDescription = worklog.Comment
                        });
                    }
                }
            }

            return excelInput;
        }

        public void ExportToExcelFile(string path, List<ExcelInputModel> excelInput)
        {
            if (!File.Exists(path))
                File.Create(path);

            var xlApp = new Application();
            if (xlApp == null)
                throw new Exception("Microsfot Excel is not properly installed.");

            Workbook xlWorkbook;
            Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkbook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = xlWorkbook.Worksheets.get_Item(1);

            xlWorkSheet.Cells[1, 1] = "Key";
            xlWorkSheet.Cells[1, 2] = "Date Started";
            xlWorkSheet.Cells[1, 3] = "Username";
            xlWorkSheet.Cells[1, 4] = "Time Spent (h)";
            xlWorkSheet.Cells[1, 5] = "Work Description";
            xlWorkSheet.Cells[1, 1].EntireRow.Font.Bold = true;

            int incr = 0;

            for (int j = 2; j < excelInput.Count() + 2; j++)
            {
                for (int i = 1; i < 6; i++)
                {
                    if (i == 1)
                    {
                        xlWorkSheet.Cells[j, i] = excelInput[incr].Key;
                    }

                    else if (i == 2)
                    {
                        xlWorkSheet.Cells[j, i].NumberFormat = "YYYY-MM-DD";
                        xlWorkSheet.Cells[j, i] = excelInput[incr].DateStarted.Date.ToString("yyyy-MM-dd");
                    }

                    else if (i == 3)
                    {
                        xlWorkSheet.Cells[j, i] = excelInput[incr].UserName;
                    }

                    else if (i == 4)
                    {
                        xlWorkSheet.Cells[j, i] = ConvertTimeString(excelInput[incr].TimeSpent);
                    }

                    else
                    {
                        xlWorkSheet.Cells[j, i] = excelInput[incr].WorkDescription;
                    }
                }

                incr++;
            }

            xlWorkSheet.UsedRange.Columns.AutoFit();

            xlWorkbook.SaveAs(path, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkbook.Close(true, misValue, misValue);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkbook);
            Marshal.ReleaseComObject(xlApp);
        }

        private static string ConvertTimeString(double time)
        {
            var ts = TimeSpan.FromSeconds(time);
            string str = ts.ToString(@"hh");
            return str;
        }
    }
}
