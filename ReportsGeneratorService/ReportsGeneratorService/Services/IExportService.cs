﻿using ReportsGeneratorService.Models;
using System;
using System.Collections.Generic;

namespace ReportsGeneratorService.Services
{
    public interface IExportService
    {
        List<ExcelInputModel> ConvertDataFromJiraToExcelInput(string userName, string password, int projectId, DateTime startDate, DateTime endDate);

        void ExportToExcelFile(string path, List<ExcelInputModel> excelInput);
    }
}
