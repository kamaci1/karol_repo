﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ApportWms.PickListModel.Models;
using PickListModel.Models;
using System.Linq;
using Newtonsoft.Json;

namespace ApportWms.Infrastructure.Services
{
    public class ApiClient : IApiClient
    {

        private static string EndpointAdress;
        public static string Token { get; set; }
        User User { get; }
        private IDataProvider _excelDataProvider = new ExcelDataProvider();

        public async Task<string> Login(string userName, string password, string backendUrl)
        {
            ApiClient.EndpointAdress = backendUrl;
            var request = await Client().PostAsJsonAsync(string.Format("http://{0}/login/signin", ApiClient.EndpointAdress), new User { Name = userName, Password = password });
            request.EnsureSuccessStatusCode();
            var reply = request.Content.ReadAsAsync<Dictionary<string, string>>().Result;
            var token = reply["Token"];
            ApiClient.Token = token;
            return token;
        }

        public async Task SignOut()
        {
            var request = await Client().GetAsync(string.Format("http://{0}/login/signout", ApiClient.EndpointAdress));
            request.EnsureSuccessStatusCode();
            ApiClient.Token = string.Empty;
        }

        public async Task SendPickListToERP(ExcelFileDescription fileDescription)
        {
            var getOrder = await _excelDataProvider.GetPickListFromDataSheet(fileDescription.ExcelPath);

            var orderHeaders = await ApiClient.Client().GetStringAsync(String.Format("http://{0}/api/SalesOrder/Headers", ApiClient.EndpointAdress));

            var orderHeadersReply = JsonConvert.DeserializeObject<List<Order>>(orderHeaders);
            if (orderHeadersReply.Any(x => x.ExternalOrderId == getOrder.SalesOrder.ExternalOrderId))
            {
                fileDescription.ErrorDetails = "Order already exists in a database";
                fileDescription.Status = "Error";
                return;
            }


            var orderReply = await ApiClient.Client().PutAsJsonAsync(
                string.Format("http://{0}/api/SalesOrder/RecieveSalesOrder", ApiClient.EndpointAdress),
                getOrder.GetSalesOrder());

            if (!orderReply.IsSuccessStatusCode)
            {
                fileDescription.Status = "Error";
                fileDescription.ErrorDetails = getOrder.OrderErrorDetails;
            }


            var preparationReply = await Client().PutAsJsonAsync(
            string.Format("http://{0}/api/ExternalPreparation/ReceiveExternalPreparation", ApiClient.EndpointAdress),
            getOrder.GetExternalPreparationInput());

            if (preparationReply.IsSuccessStatusCode)
            {
                fileDescription.Status = "OK";
            }
            else
            {
                fileDescription.Status = "Error";
            }
        }

        public static HttpClient Client()
        {
            var client = new HttpClient() { Timeout = new TimeSpan(100, 0, 0) };
            if (!string.IsNullOrWhiteSpace(Token))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            }

            return client;
        }
    }
}
