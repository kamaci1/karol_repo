﻿using Excel;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Threading.Tasks;
using ApportWms.PickListModel.Models;
using PickListModel.Models;
using System;

namespace ApportWms.Infrastructure.Services
{
    public class ExcelDataProvider : IDataProvider
    {
        public async Task<ExcelFilePickList> GetPickListFromDataSheet(string fileName)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(fileName);
                return await this.GetPickListFromFile(fileInfo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task<ExcelFilePickList> GetPickListFromFile(FileInfo file)
        {
            using (FileStream stream = file.OpenRead())
            {
                using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
                {
                    DataSet ds = excelReader.AsDataSet();
                    DataTable dt = ds.Tables[0];
                    DataRow dr;

                    //Declaring which rows to iterate through
                    int rowStart = 0;
                    int rowEnd = dt.Rows.Count; // returns the exact number of filled rows
                    List<ExcelFilePickList> ordersList = new List<ExcelFilePickList>();
                    var excelPickList = new ExcelFilePickList();
                    var excelOrderFile = new ExcelFileDescription();
                    excelPickList.SalesOrder = new SalesOrder();
                    excelOrderFile.ExcelPath = file.FullName;
                    var readOrderLines = false;
                    var orderLinesList = new List<OrderLine>();

                    for (int rowNum = rowStart; rowNum < rowEnd; rowNum++)
                    {
                        dr = dt.Rows[rowNum];
                        if (dr.IsNull(0))
                        {
                            continue;
                        }
                        //data for external preparation
                        if (!readOrderLines)
                        {
                            for (int i = 0; i <= 5; i++)
                            {
                                if (dr.ItemArray[i].ToString() == "OrderNumber" && rowNum + 1 < rowEnd)
                                {
                                    var orderNo = excelPickList.SalesOrder.OrderNumber = dt.Rows[rowNum + 1][i].ToString();
                                    if (String.IsNullOrWhiteSpace(orderNo))
                                    {
                                        excelPickList.OrderErrorDetails = "Missing orderNumber";
                                        break;
                                    }
                                }
                                else if (dr.ItemArray[i].ToString() == "OrderType" && rowNum + 1 < rowEnd)
                                {
                                    var orderType = excelPickList.SalesOrder.OrderType = dt.Rows[rowNum + 1][i].ToString();
                                    if (orderType != "ErpProcessed")
                                    {
                                        excelPickList.OrderErrorDetails = "Wrong orderType";
                                        break;
                                    }
                                }
                                else if (dr.ItemArray[i].ToString() == "ERPOrderId" && rowNum + 1 < rowEnd)
                                {
                                    var erpOrderId = excelPickList.SalesOrder.ExternalOrderId = dt.Rows[rowNum + 1][i].ToString();
                                    if (String.IsNullOrWhiteSpace(erpOrderId))
                                    {
                                        excelPickList.OrderErrorDetails = "Wrong ERPOrderId";
                                        break;
                                    }
                                }
                                else if (dr.ItemArray[i].ToString() == "OrderComment" && rowNum + 1 < rowEnd)
                                {
                                    excelPickList.SalesOrder.Information = new List<string> { dt.Rows[rowNum + 1][i].ToString() };
                                }
                                else if (dr.ItemArray[i].ToString() == "CustomerName" && rowNum + 1 < rowEnd)
                                {
                                    excelPickList.SalesOrder.CustomerName = dt.Rows[rowNum + 1][i].ToString();
                                }
                                else if (dr.ItemArray[i].ToString() == "OrderLineId")
                                {
                                    readOrderLines = true;
                                }
                            }
                        }

                        //data for orderLines
                        if (readOrderLines && dr.ItemArray.Length >= 5)
                        {

                            var orderLine = new OrderLine
                            {
                                MaterialDescription = dr.ItemArray[2].ToString(),
                                MaterialName = dr.ItemArray[1].ToString(),
                                MaterialInformation = dr.ItemArray[4].ToString(),
                                LocationName = dr.ItemArray[5].ToString()
                            };

                            decimal quantity;
                            long orderLineId;

                            if (long.TryParse(dr.ItemArray[0].ToString(), out orderLineId))
                            {
                                orderLine.ExternalOrderLineID = orderLineId;
                            }
                            if (decimal.TryParse(dr.ItemArray[3].ToString(), out quantity))
                            {
                                orderLine.Quantity = quantity;
                            }

                            if (orderLine.ExternalOrderLineID > 0)
                            {
                                orderLinesList.Add(orderLine);
                            }
                        }
                    }
                    excelReader.Close();
                    excelPickList.SalesOrder.OrderLines = orderLinesList.ToArray();
                    return await Task.FromResult(excelPickList);
                }
            }
        }

        //private async Task<ExcelFilePickList> GetDataFromFile(FileInfo file)
        //{
        //    //open excel file
        //    //using (FileStream stream = file.OpenRead())
        //    //{
        //    //    using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream))
        //    //    {
        //    //        DataSet ds = excelReader.AsDataSet();
        //    //        DataTable dt = ds.Tables[0];
        //    //        DataRow dr;

        //    //        //Declaring which rows to iterate through
        //    //        int rowStart = 0;
        //    //        int rowEnd = dt.Rows.Count; //returns the number of filled rows
        //    //        List<ExcelFilePickList> ordersList = new List<ExcelFilePickList>();
        //    //        var excelOrder = new ExcelFilePickList();

        //    //        for (int rowNum = rowStart; rowNum <= rowEnd; rowNum++)
        //    //        {
        //    //            dr = dt.Rows[rowNum];
        //    //            if (dr.IsNull(0))
        //    //            {
        //    //                continue;
        //    //            }

        //    //            for (int i = 0; i <= 5; i++)
        //    //            {
        //    //                if (dr.ItemArray[i].ToString() == "OrderNumber")
        //    //                {
        //    //                    if (rowNum + 1 < rowEnd)
        //    //                    {
        //    //                        var columnData = dt.Rows[rowNum + 1][i].ToString();
        //    //                        var salesOrder = new SalesOrder();
        //    //                        salesOrder.OrderNumber = columnData;
        //    //                        excelOrder.SalesOrder = salesOrder;
        //    //                        excelOrder.ExcelPath = file.FullName;
        //    //                        break;
        //    //                    }
        //    //                }
        //    //            }
        //    //            if (excelOrder.SalesOrder != null)
        //    //                break;
        //    //        }

        //    //        excelReader.Close();
        //    //        return await Task.FromResult(excelOrder);
        //    //    }
        //    //}
        //    return null;
        //}
    }
}
