﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApportWms.Infrastructure.Services.Extensions
{
    public interface IDataSorter
    {
        string[,] FormatOrderData(List<object> inputData);
    }
}
