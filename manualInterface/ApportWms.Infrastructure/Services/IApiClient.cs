﻿using PickListModel.Models;
using System.Threading.Tasks;

namespace ApportWms.Infrastructure.Services
{
    public interface IApiClient
    {
        Task<string> Login(string username, string password, string backendUrl);
        Task SignOut();
        Task SendPickListToERP(ExcelFileDescription fileDescription);
    }
}
