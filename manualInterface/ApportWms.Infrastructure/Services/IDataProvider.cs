﻿using PickListModel.Models;
using System.Threading.Tasks;

namespace ApportWms.Infrastructure.Services
{
    public interface IDataProvider
    {
        Task<ExcelFilePickList> GetPickListFromDataSheet(string fileName);
    }
}
