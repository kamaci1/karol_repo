﻿using System;

namespace ApportWms.Interface.ViewModels
{
    public interface ILoginViewModel
    {
        bool IsAuthenticated { get; set; }

        event EventHandler AuthenticationChanged;

    }
}
