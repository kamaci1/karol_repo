﻿using PickListModel.Models;
using System.Collections.Generic;

namespace ApportWms.Interface.ViewModels
{
    public interface IOrdersViewModel
    {
        List<ExcelFileOrder> Orders { get; set; }
    }
}
