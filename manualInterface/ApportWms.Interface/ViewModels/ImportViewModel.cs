﻿using ApportWms.Infrastructure.Services;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PickListModel.Models;
using System.Linq;
using System.Windows.Forms;
using System.Threading.Tasks;
using visb = System.Windows;

namespace ApportWms.Interface.ViewModels
{
    public class ImportViewModel : PropertyChangedBase, IImportViewModel
    {
        private ILoginViewModel _loginView = new LoginViewModel();
        private IApiClient _client = new ApiClient();

        public ImportViewModel()
        {
        }

        private ObservableCollection<ExcelFileDescription> _orders;
        public ObservableCollection<ExcelFileDescription> Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                _orders = value;
                NotifyOfPropertyChange(() => Orders);
            }
        }

        private bool _isOrdersSelected;
        public bool IsOrdersSelected
        {
            get
            {
                return _isOrdersSelected;
            }
            set
            {
                _isOrdersSelected = value;
                NotifyOfPropertyChange(() => IsOrdersSelected);
                NotifyOfPropertyChange(() => CanImportFiles);
            }
        }

        public void DisplayOrderFiles()
        {
            var ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            ofd.Multiselect = true;
            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                var orderList = new List<ExcelFileDescription>();
                IsOrdersSelected = false;                
                foreach (var file in ofd.FileNames)
                {
                    var excelFile = new ExcelFileDescription();
                    excelFile.ExcelPath = file;
                    excelFile.Status = "Ready";
                    orderList.Add(excelFile);
                }                
                Orders = new ObservableCollection<ExcelFileDescription>(orderList);
                IsOrdersSelected = true;

            }
            else
            {
                MessageBox.Show("You have to pick a file or files");
            }
        }

        public bool CanImportFiles { get { return IsOrdersSelected; } }
        public async void ImportFiles()
        {
            ImportInProgress = true;
            var excelPickList = new List<ExcelFileDescription>();
            IsOrdersSelected = false;            
            var tasks = Orders.Select(x => _client.SendPickListToERP(x));         
            await Task.WhenAll(tasks);
            ImportInProgress = false;
        }

        bool _importInProgress;
        public bool ImportInProgress
        {
            get
            {
                return _importInProgress;
            }
            set
            {
                _importInProgress = value;
                NotifyOfPropertyChange(() => ImportInProgress);
                NotifyOfPropertyChange(() => CurrentImportProgress);
            }
        }

        public visb.Visibility CurrentImportProgress
        {
            get
            {
                return ImportInProgress ? visb.Visibility.Visible : visb.Visibility.Collapsed;
            }
        }
    }
}
