﻿using ApportWms.Infrastructure.Services;
using Caliburn.Micro;
using System;
using System.Configuration;
using System.Windows;

namespace ApportWms.Interface.ViewModels
{
    public class LoginViewModel : PropertyChangedBase, ILoginViewModel
    {
        IApiClient _loginService = new ApiClient();                

        public LoginViewModel()
        {
            _BackendUrl = ConfigurationManager.AppSettings["backendUrl"];
        }

        string _BackendUrl;
        public string BackendUrl
        {
            get
            {
                return _BackendUrl;
            }
            set
            {
                if (value != _BackendUrl)
                {
                    _BackendUrl = value;
                    NotifyOfPropertyChange(BackendUrl);
                }
            }
        }

        string _UserName = "admin";
        public string UserName 
        {
            get
            {
                return _UserName;
            }
            set
            {
                if (value != _UserName)
                {
                    _UserName = value;
                    NotifyOfPropertyChange(() => UserName);
                }
            }
        }

        string _Password = "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918";
        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                if (value != _Password)
                {
                    _Password = value;
                    NotifyOfPropertyChange(() => Password);
                }
            }
        }

        public event EventHandler AuthenticationChanged;

        private bool _IsAuthenticated;
        public bool IsAuthenticated
        {
            get { return _IsAuthenticated; }
            set
            {
                _IsAuthenticated = value;
                NotifyOfPropertyChange(() => IsAuthenticated);
                AuthenticationChanged?.Invoke(this, new EventArgs());
            }
        }

        private object _content;

        public object Content
        {
            get { return _content; }
            set
            {
                _content = value;
                NotifyOfPropertyChange(() => Content);
            }
        }

        public async void DoLogin()
        {
            if (string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password))
            {
                ErrorMessage = "UserName or password is empty";                  
                return;
            }
            try
            {
                if (IsAuthenticated)
                {
                    SignOut();
                }

                var Token = await _loginService.Login(UserName, Password, BackendUrl);
                if (!String.IsNullOrWhiteSpace(Token))
                {
                    IsAuthenticated = true;                    
                    AuthenticationChanged += LoginViewModel_AuthenticationChanged;
                    ErrorMessage = string.Empty;
                    IsPropertyReadOnly = true;
                    IsPasswordEnabled = false;                               
                }                                
            }
            catch (System.Exception)
            {
                IsAuthenticated = false;
                ErrorMessage = "Invalid login credentials or server error";
            }
        }

        public void SignOut()
        {
            try
            {
                IsAuthenticated = false;
                IsPropertyReadOnly = false;
                IsPasswordEnabled = true;
                _loginService.SignOut();
                AuthenticationChanged -= LoginViewModel_AuthenticationChanged;
                UserName = string.Empty;
                Password = string.Empty;
            }
            catch (Exception)
            {
                throw;
            }

        }

        private void LoginViewModel_AuthenticationChanged(object sender, EventArgs e)
        {
            NotifyOfPropertyChange(() => DisableSignOut);
            NotifyOfPropertyChange(() => DisableLogin);                      
        }

        public Visibility DisableSignOut
        {
            get
            {
                return IsAuthenticated ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility DisableLogin
        {
            get
            {
                return !IsAuthenticated ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        bool _isReadOnly; // indicates whether the property is readonly
        public bool IsPropertyReadOnly
        {
            get
            {
                return _isReadOnly;
            }
            set
            {
                _isReadOnly = value;
                NotifyOfPropertyChange(() => IsPropertyReadOnly);                
            }
        }

        string _errorMessage; //Simple credentials validation messages
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                NotifyOfPropertyChange(() => ErrorMessage);
            }
        }

        bool _isPasswordEnabled = true;
        public bool IsPasswordEnabled
        {
            get
            {
                return _isPasswordEnabled;
            }
            set
            {
                _isPasswordEnabled = value;
                NotifyOfPropertyChange(() => IsPasswordEnabled);
            }
        }
    }
}
