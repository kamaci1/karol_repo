﻿using Caliburn.Micro;
using System;
using System.Windows;

namespace ApportWms.Interface.ViewModels
{
    public class MainWindowViewModel : PropertyChangedBase, IMainWindowViewModel
    {
        private IImportViewModel _importView = new ImportViewModel();
        private ILoginViewModel _loginView;

        public MainWindowViewModel()
        {
            _loginView = new LoginViewModel();
            _loginView.AuthenticationChanged += _loginView_AuthenticationChanged;

            MainContent = _loginView;
        }

        private void _loginView_AuthenticationChanged(object sender, EventArgs e)
        {
            NotifyOfPropertyChange(() => DisableContent);
            if (_loginView.IsAuthenticated)
            {
                ShowImports();
            }          
        }

        private object _mainContent;

        public object MainContent
        {
            get { return _mainContent; }
            set
            {
                _mainContent = value;
                NotifyOfPropertyChange(() => MainContent);
            }
        }

        public void ShowSettings()
        {
            MainContent = _loginView;
        }

        public void ShowImports()
        {
            MainContent = _importView;
        }

        public Visibility DisableContent
        {
            get
            {                
                return _loginView.IsAuthenticated ? Visibility.Visible : Visibility.Collapsed;
            }
        }    
    }
}
