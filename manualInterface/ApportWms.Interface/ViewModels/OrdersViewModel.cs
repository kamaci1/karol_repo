﻿using ApportWms.Infrastructure.Services;
using Caliburn.Micro;
using Microsoft.Win32;
using PickListModel.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ApportWms.Interface.ViewModels
{
    public class OrdersViewModel : PropertyChangedBase, IOrdersViewModel
    {
        private ISimulatorService _service = new SimulatorService();

        public OrdersViewModel()
        {
        }
        private List<ExcelFileOrder> _orders;
        public List<ExcelFileOrder> Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                _orders = value;
                NotifyOfPropertyChange(() => Orders);
            }
        }

        public void DisplayOrderFiles()
        {
            /*
             * Open windows file selct in the viewmodel
             * Select .excel files
             * Give file selection to the IDataProvider
             * Get ExcelFileOrder list from data provider
             * Show ExcelFileOrder list 
             */
            var ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            ofd.Multiselect = true;
            ofd.ShowDialog();
            var orderList = new List<ExcelFileOrder>();

            foreach (var file in ofd.FileNames)
            {
                var order = _service.SendPickListFromFile(file);
                orderList.Add(order);
            }
            Orders = orderList;
            OrdersDisplayed = true;
        }

        public bool _ordersDisplayed;
        public bool OrdersDisplayed
        {
            get
            {
                return _ordersDisplayed;
            }
            set
            {
                if (Orders.Count != 0)
                {
                    _ordersDisplayed = value;
                    NotifyOfPropertyChange(() => OrdersDisplayed);
                }
            }
        }
    }
}
