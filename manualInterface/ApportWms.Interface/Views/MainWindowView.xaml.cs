﻿using ApportWms.Interface.ViewModels;
using Caliburn.Micro;
using MahApps.Metro.Controls;
using System.Windows;

namespace ApportWms.Interface.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : MetroWindow
    {

        public MainWindowView()
        {
        }
    }
}
