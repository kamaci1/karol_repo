﻿using Caliburn.Micro;
using System.IO;

namespace PickListModel.Models
{
    public class ExcelFileDescription : PropertyChangedBase
    {
        public string ExcelPath;

        public string ExcelFileName
        {
            get
            {
                return !string.IsNullOrEmpty(ExcelPath) ? Path.GetFileName(ExcelPath) : string.Empty;
            }
        }

        string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        string _errorDetails;
        public string ErrorDetails
        {
            get
            {
                return _errorDetails;
            }
            set
            {
                _errorDetails = value;
                NotifyOfPropertyChange(() => ErrorDetails);
            }
        }
    }
}
