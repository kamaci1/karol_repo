﻿using ApportWms.PickListModel.Models;
using System.Linq;

namespace PickListModel.Models
{
    public class ExcelFilePickList
    {        
        public string OrderNumber { get { return SalesOrder.OrderNumber; } }

        public SalesOrder SalesOrder;

        public ExternalPreparationInput GetExternalPreparationInput()
        {           
            return new ExternalPreparationInput
            {
                ExternalOrderId = SalesOrder?.ExternalOrderId,
                PreparationLines = SalesOrder?.OrderLines.Select(x => 
                new ExternalPreparationLineInput{
                    ExternalOrderLineID = x.ExternalOrderLineID.ToString(),
                    LocationName = x.LocationName,
                    Quantity = x.Quantity

                }).ToArray()
            };            
        }

        public SalesOrder GetSalesOrder()
        {
            return SalesOrder;
        }

        public string OrderErrorDetails { get; set; }
    }
}
// status should be return message either succes or failure
// after i send the files the statuses should be updated in the datagrid through notifyofpropertychange

    //clean up the code 
    //refactor it 