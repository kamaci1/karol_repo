﻿namespace PickListModel.Models
{
    public class ExternalPreparationInput
    {
        /// <summary>
         /// Unique external orderID, used when Apport communicates with ERP about the order
         /// </summary>
        public string ExternalOrderId { get; set; }
        public ExternalPreparationLineInput[] PreparationLines { get; set; }
    }
}
