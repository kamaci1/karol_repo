﻿namespace PickListModel.Models
{
    public class ExternalPreparationLineInput
    {
        //Why this value is string? In the ERP documentation is int
        public string ExternalOrderLineID { get; set; }
        public string LocationName { get; set; }
        public decimal Quantity { get; set; }
    }
}
