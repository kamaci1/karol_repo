﻿using System.Collections.Generic;

namespace ApportWms.PickListModel.Models
{
    public class Order
    {
        /// <summary>
        /// Unique external orderID, used when Apport communicates with ERP about the order
        /// </summary>
        public string ExternalOrderId { get; set; }

        /// <summary>
        /// Is by apport to determin how to process the order
        /// </summary>
        public string OrderType { get; set; }

        /// <summary>
        /// The order number of the order that is presented to users of ApportWMS
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Freetext order information that can be presented to users of ApportWMS
        /// </summary>
        public List<string> Information { get; set; }

        public OrderLine[] OrderLines { get; set; }
    }
}
