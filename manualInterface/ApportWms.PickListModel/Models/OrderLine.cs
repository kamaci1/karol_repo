﻿namespace ApportWms.PickListModel.Models
{
    public class OrderLine
    {
        /// <summary>
        /// Unique external orderID, used when Apport communicates with ERP about the order
        /// </summary>
        public long ExternalOrderLineID { get; set; }

        /// <summary>
        /// Material name that identifies the material
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// A short product name that describes what the material actually is
        /// </summary>
        public string MaterialDescription { get; set; }

        /// <summary>
        /// Additional information for the material.
        /// </summary>
        public string MaterialInformation { get; set; }

        /// <summary>
        /// Positive value for the required number of items in base unit
        /// </summary>
        public decimal Quantity { get; set; }

        // Add treaceability req

        /// <summary>
        /// Freetext order information that can be presented to users of ApportWMS
        /// </summary>
        public string LocationName { get; set; }
    }
}
