﻿namespace ApportWms.PickListModel.Models
{
    public class SalesOrder : Order
    {
        /// <summary>
        /// Recipient of the order
        /// </summary>
        public string CustomerName { get; set; }
    }
}
